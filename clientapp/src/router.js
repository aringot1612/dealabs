import dealsItem from './components/deals.vue';
import dealsDetails from "./components/dealsDetails.vue";
import addDealItem from "./components/addDeal.vue";
import signin from "./components/signin.vue";
import signup from "./components/signup.vue";
import VueRouter from 'vue-router'

export default new VueRouter({
    mode: 'history',
    routes: [
        {path: "/", component: dealsItem},
        {path: "/deal/new", component: addDealItem},
        {path: "/deal/:id", component: dealsDetails},
        {path: "/signup", component: signup},
        {path: "/signin", component: signin}
    ]
});