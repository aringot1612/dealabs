import axios from 'axios';

export default function execute() {
    axios.interceptors.request.use(async function (config) {
        if (!config.url.includes('public')) {
            config.headers['Authorization'] = 'Basic ' + localStorage.getItem('auth');
        }
        return config
    }, function (error) {
        return Promise.reject(error)
    });
}