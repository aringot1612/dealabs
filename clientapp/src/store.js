import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


const store = new Vuex.Store({
    state: {
        connected: localStorage.getItem('auth') != null ? 1 : 0,
        retrieveData: 0,
        showSignIn: 0,
        identifier: localStorage.getItem('id') != null ? localStorage.getItem('id') : ''
    },
    mutations: {
        login() {
            this.state.connected = 1;
        },
        identify(state, id) {
            this.state.identifier = id;
        },
        logout() {
            this.state.connected = 0;
        },
        exeRetrieveData() {
            this.state.retrieveData = !this.state.retrieveData;
        },
        exeShowSignIn() {
            this.state.showSignIn = !this.state.showSignIn;
        }
    }
})
export default store;