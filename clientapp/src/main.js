import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import vuetify from './plugins/vuetify'
import router from './router.js';
import interceptor from './helpers/interceptor.js';
import Vuex from 'vuex'
import 'es6-promise/auto'
import store from './store';

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(Vuex);
interceptor();


new Vue({
    vuetify,
    router: router,
    store,
    render: h => h(App)
}).$mount('#app')
