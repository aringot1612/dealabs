# Dealabs

## Description

Projet étudiant M2 : Dealabs

## Auteur

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçu web
### Page d'accueil
<br>![Aperçu : Page d'accueil](images/homepage.png)

### Page d'accueil (utilisateur connecté)
<br>![Aperçu : Page d'accueil (utilisateur connecté)](images/homepage_logged.png)

### Inscription
<br>![Aperçu : Inscription](images/signup.png)

### Connexion
<br>![Aperçu : Connexion](images/signin.png)

### Détails d'un deal
<br>![Aperçu : Détails d'un deal](images/dealExtended.png)

### Ajout d'un deal
<br>![Aperçu : Ajout d'un deal](images/addDeal.png)