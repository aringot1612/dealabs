package fr.univlittoral.dealabs.mappers;

import fr.univlittoral.dealabs.dto.DetailedDealDTO;
import fr.univlittoral.dealabs.entity.DealDO;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DealMapper {
    public DealDO map(final DetailedDealDTO deal) {
        final DealDO newDeal = new DealDO();
        newDeal.setTitle(deal.getTitle());
        newDeal.setShopName(deal.getShopName());
        newDeal.setShopLink(deal.getShopLink());
        newDeal.setPriceOld(Double.parseDouble(deal.getOldPrice()));
        newDeal.setPriceNew(Double.parseDouble(deal.getNewPrice()));
        newDeal.setPromoCode(deal.getPromoCode());
        newDeal.setDate(LocalDateTime.now());
        newDeal.setImgUrl(deal.getImageUrl());
        newDeal.setDescription(deal.getDescription());
        return newDeal;
    }

    public DetailedDealDTO map(final DealDO deal, final int temperature, final double discount, final boolean full) {
        final DetailedDealDTO newDeal = new DetailedDealDTO();
        newDeal.setId(deal.getId());
        newDeal.setTitle(deal.getTitle());
        newDeal.setAuthor(deal.getCreator().getUsername());
        newDeal.setShopName(deal.getShopName());
        newDeal.setTemperature(temperature);
        newDeal.setImageUrl(deal.getImgUrl());
        newDeal.setCreationDate(deal.getDate().toString());
        newDeal.setShopLink(deal.getShopLink());
        if (full) {
            newDeal.setPromoCode(deal.getPromoCode());
            newDeal.setDescription(deal.getDescription());
            newDeal.setOldPrice(Double.toString(deal.getPriceOld()));
            newDeal.setNewPrice(Double.toString(deal.getPriceNew()));
            newDeal.setDiscountPercentage(discount);
        }
        return newDeal;
    }
}
