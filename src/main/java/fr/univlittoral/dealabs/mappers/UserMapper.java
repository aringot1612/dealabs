package fr.univlittoral.dealabs.mappers;

import fr.univlittoral.dealabs.dto.UserDTO;
import fr.univlittoral.dealabs.entity.UserDO;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserDO map(final UserDTO user) {
        final UserDO newUser = new UserDO();
        newUser.setUsername(user.getUserName());
        newUser.setPwd(user.getPassword());
        newUser.setLastName(user.getLastName());
        newUser.setFirstName(user.getFirstName());
        return newUser;
    }
}
