package fr.univlittoral.dealabs.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Extends du user spring (org.springframework.security.core.userdetails.User) si besoin d'ajout d'informations
 *
 * @author Max
 */
public class LoggedUserDTO extends User {
    private static final long serialVersionUID = -2836522345185404025L;

    public LoggedUserDTO(final String username, final String password, final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}