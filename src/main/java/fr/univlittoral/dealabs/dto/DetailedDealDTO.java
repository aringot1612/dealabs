package fr.univlittoral.dealabs.dto;

public class DetailedDealDTO extends DealDTO {
    private String promoCode;
    private String oldPrice;
    private String newPrice;
    private double discountPercentage;
    private String description;
    private String creator;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(final String promoCode) {
        this.promoCode = promoCode;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(final String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(final String newPrice) {
        this.newPrice = newPrice;
    }

    public double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(final double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(final String creator) {
        this.creator = creator;
    }
}
