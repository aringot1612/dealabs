package fr.univlittoral.dealabs.dto;

public class TemperatureDTO {
    private long dealId;
    private String userName;

    public long getDealId() {
        return dealId;
    }

    public void setDealId(final long dealId) {
        this.dealId = dealId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }
}
