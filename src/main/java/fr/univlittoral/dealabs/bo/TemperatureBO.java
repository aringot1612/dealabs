package fr.univlittoral.dealabs.bo;

import fr.univlittoral.dealabs.dto.TemperatureDTO;
import fr.univlittoral.dealabs.services.TemperatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemperatureBO {
    @Autowired
    private TemperatureService temperatureService;

    public boolean savePositive(final TemperatureDTO temperatureDTO) {
        return temperatureService.savePositive(temperatureDTO);
    }

    public boolean saveNegative(final TemperatureDTO temperatureDTO) {
        return temperatureService.saveNegative(temperatureDTO);
    }
}
