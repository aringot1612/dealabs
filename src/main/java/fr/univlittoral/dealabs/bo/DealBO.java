package fr.univlittoral.dealabs.bo;

import fr.univlittoral.dealabs.dto.DealDTO;
import fr.univlittoral.dealabs.dto.DetailedDealDTO;
import fr.univlittoral.dealabs.entity.DealDO;
import fr.univlittoral.dealabs.entity.UserDO;
import fr.univlittoral.dealabs.mappers.DealMapper;
import fr.univlittoral.dealabs.repositories.IDealRepository;
import fr.univlittoral.dealabs.services.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class DealBO {
    @Autowired
    private IDealRepository dealRepository;
    @Autowired
    private DealMapper dealMapper;
    @Autowired
    private DealService dealService;

    /**
     * Retourne une liste de DTO (deals) pour l'affichage principal des deals en page d'accueil.
     *
     * @return Une liste de DTO.
     */
    public List<DealDTO> findAll() {
        final List<DealDO> dealsDo = dealRepository.findAll();
        final List<DealDTO> deals = new ArrayList<>();
        for (final DealDO deal : dealsDo) {
            deals.add(dealMapper.map(deal, dealService.computeTemperature(deal.getTemperatures()), dealService.computeDiscount(deal.getPriceOld(), deal.getPriceNew()), false));
        }
        Collections.sort(deals, Comparator.comparing(DealDTO::getCreationDate).reversed());
        return deals;
    }

    public DetailedDealDTO findOne(final long id) {
        final DealDO deal = dealRepository.getById(id);
        return dealMapper.map(deal, dealService.computeTemperature(deal.getTemperatures()), dealService.computeDiscount(deal.getPriceOld(), deal.getPriceNew()), true);
    }

    public long save(final DetailedDealDTO deal) {
        final DealDO dealToSave = dealMapper.map(deal);
        final UserDO userDO = dealService.findUserForPost(deal.getCreator());
        if (userDO != null) {
            dealToSave.setCreator(userDO);
            return dealRepository.save(dealToSave).getId();
        } else
            return -1;
    }

    public int getTemperature(final long id) {
        final DealDO deal = dealRepository.getById(id);
        return dealService.computeTemperature(deal.getTemperatures());
    }
}
