package fr.univlittoral.dealabs.bo;

import fr.univlittoral.dealabs.dto.LoggedUserDTO;
import fr.univlittoral.dealabs.dto.LoginRequestDTO;
import fr.univlittoral.dealabs.dto.UserDTO;
import fr.univlittoral.dealabs.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class UserBO {
    @Autowired
    private UserService userService;

    public long save(final UserDTO user) {
        return userService.save(user);
    }

    public LoggedUserDTO login(@RequestBody final LoginRequestDTO request) {
        return userService.login(request);
    }
}
