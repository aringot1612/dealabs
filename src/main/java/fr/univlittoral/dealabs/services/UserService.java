package fr.univlittoral.dealabs.services;

import fr.univlittoral.dealabs.dto.LoggedUserDTO;
import fr.univlittoral.dealabs.dto.LoginRequestDTO;
import fr.univlittoral.dealabs.dto.UserDTO;
import fr.univlittoral.dealabs.entity.UserDO;
import fr.univlittoral.dealabs.mappers.UserMapper;
import fr.univlittoral.dealabs.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class UserService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordService passwordService;
    @Autowired
    private AuthenticationProvider authenticationManager;

    public long save(final UserDTO user) {
        final UserDO userDO = userMapper.map(user);
        userDO.setPwd(passwordService.encode(user.getPassword()));
        return userRepository.save(userDO).getId();
    }

    public LoggedUserDTO login(@RequestBody final LoginRequestDTO request) {
        if (request.getUserName().isEmpty() || request.getPassword().isEmpty())
            throw new BadCredentialsException("User/pwd must not be emtpy");
        final Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));
        if (authentication == null)
            throw new BadCredentialsException("User/pwd incorrect");
        final LoggedUserDTO utilisateur = (LoggedUserDTO) authentication.getPrincipal();
        return new LoggedUserDTO(utilisateur.getUsername(), utilisateur.getPassword(), utilisateur.getAuthorities());
    }
}
