package fr.univlittoral.dealabs.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * Le password correspond il à celui crypté ?
     *
     * @param BCryptFormat : Crypté
     * @param rawFormat    : Clair
     * @return
     */
    public Boolean matches(final String rawFormat, final String BCryptFormat) {
        return passwordEncoder.matches(rawFormat, BCryptFormat);
    }

    /**
     * Encode le mot de passe
     *
     * @param BCryptFormat
     * @param textFormat
     * @return
     */
    String encode(final String textFormat) {
        return passwordEncoder.encode(textFormat);
    }
}
