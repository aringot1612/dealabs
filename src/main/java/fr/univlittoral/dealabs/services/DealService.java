package fr.univlittoral.dealabs.services;

import fr.univlittoral.dealabs.entity.TemperatureDO;
import fr.univlittoral.dealabs.entity.UserDO;
import fr.univlittoral.dealabs.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class DealService {
    @Autowired
    private IUserRepository userRepository;

    public double computeDiscount(final double oldPrice, final double newPrice) {
        BigDecimal bd = BigDecimal.valueOf((newPrice * 100) / oldPrice);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public int computeTemperature(final List<TemperatureDO> temperatures) {
        int temperature = 0;
        for (final TemperatureDO temperatureDO : temperatures) {
            temperature += temperatureDO.getValue();
        }
        return temperature;
    }

    public UserDO findUserForPost(final String userName) {
        return userRepository.findDistinctByUsername(userName);
    }
}
