package fr.univlittoral.dealabs.services;

import fr.univlittoral.dealabs.dto.TemperatureDTO;
import fr.univlittoral.dealabs.entity.DealDO;
import fr.univlittoral.dealabs.entity.TemperatureDO;
import fr.univlittoral.dealabs.entity.UserDO;
import fr.univlittoral.dealabs.repositories.IDealRepository;
import fr.univlittoral.dealabs.repositories.ITemperatureRepository;
import fr.univlittoral.dealabs.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemperatureService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IDealRepository dealRepository;
    @Autowired
    private ITemperatureRepository temperatureRepository;

    public boolean savePositive(final TemperatureDTO temperature) {
        final UserDO user = userRepository.findDistinctByUsername(temperature.getUserName());
        final DealDO deal = dealRepository.getById(temperature.getDealId());
        final TemperatureDO newTemperature = temperatureRepository.save(createTemperature(user, deal, 1));
        return newTemperature != null;
    }

    public boolean saveNegative(final TemperatureDTO temperature) {
        final UserDO user = userRepository.findDistinctByUsername(temperature.getUserName());
        final DealDO deal = dealRepository.getById(temperature.getDealId());
        final TemperatureDO newTemperature = temperatureRepository.save(createTemperature(user, deal, -1));
        return newTemperature != null;
    }

    private TemperatureDO createTemperature(final UserDO user, final DealDO deal, final int value) {
        final TemperatureDO temperature = new TemperatureDO();
        temperature.setUser(user);
        temperature.setDeal(deal);
        temperature.setValue(value);
        return temperature;
    }
}
