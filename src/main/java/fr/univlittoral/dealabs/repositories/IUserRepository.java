package fr.univlittoral.dealabs.repositories;

import fr.univlittoral.dealabs.entity.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.REQUIRED)
public interface IUserRepository extends JpaRepository<UserDO, Long> {
    @Query
    UserDO findDistinctByUsername(final String userName);
}
