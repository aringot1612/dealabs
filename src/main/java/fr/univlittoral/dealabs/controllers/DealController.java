package fr.univlittoral.dealabs.controllers;

import fr.univlittoral.dealabs.bo.DealBO;
import fr.univlittoral.dealabs.dto.DealDTO;
import fr.univlittoral.dealabs.dto.DetailedDealDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/deal")
@CrossOrigin(origins = "http://localhost:8081")
public class DealController {
    @Autowired
    private DealBO dealBO;

    /**
     * Est appelé via une requête GET sur la route /rest/deals.
     *
     * @return Un objet json contenant des DTO pour l'affichage principal.
     */
    @GetMapping(path = "public")
    public List<DealDTO> findAll() {
        return dealBO.findAll();
    }

    @GetMapping(path = "public/{id}")
    public DetailedDealDTO findOne(@PathVariable final long id) {
        return dealBO.findOne(id);
    }

    @PostMapping()
    public long save(@RequestBody final DetailedDealDTO deal) {
        return dealBO.save(deal);
    }

    @GetMapping(path = "public/{id}/temperature")
    public int getTemperature(@PathVariable final long id) {
        return dealBO.getTemperature(id);
    }
}
