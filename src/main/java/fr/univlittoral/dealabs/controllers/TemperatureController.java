package fr.univlittoral.dealabs.controllers;

import fr.univlittoral.dealabs.bo.TemperatureBO;
import fr.univlittoral.dealabs.dto.TemperatureDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/temperature")
@CrossOrigin(origins = "http://localhost:8081")
public class TemperatureController {
    @Autowired
    private TemperatureBO temperatureBO;

    /**
     * Est appelé via une requête GET sur la route /rest/deals.
     *
     * @return Un objet json contenant des DTO pour l'affichage principal.
     */
    @PostMapping(path = "plus")
    public boolean savePositive(@RequestBody final TemperatureDTO temperatureDTO) {
        return temperatureBO.savePositive(temperatureDTO);
    }

    @PostMapping(path = "minus")
    public boolean saveNegative(@RequestBody final TemperatureDTO temperatureDTO) {
        return temperatureBO.saveNegative(temperatureDTO);
    }
}
