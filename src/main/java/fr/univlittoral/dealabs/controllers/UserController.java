package fr.univlittoral.dealabs.controllers;

import fr.univlittoral.dealabs.bo.UserBO;
import fr.univlittoral.dealabs.dto.LoggedUserDTO;
import fr.univlittoral.dealabs.dto.LoginRequestDTO;
import fr.univlittoral.dealabs.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/user/public")
@CrossOrigin(origins = "http://localhost:8081")
public class UserController {
    @Autowired
    private UserBO userBO;

    @PostMapping()
    public long save(@RequestBody final UserDTO user) {
        return userBO.save(user);
    }

    /**
     * methode de connexion d'un utilisateur
     *
     * @param request données nécessaire a la connexion
     * @return UserInfo LoggedUserDTO
     */
    @PostMapping(path = "login")
    public LoggedUserDTO login(@RequestBody final LoginRequestDTO request) {
        return userBO.login(request);
    }
}
