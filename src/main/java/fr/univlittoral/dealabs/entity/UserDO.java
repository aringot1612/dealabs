package fr.univlittoral.dealabs.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_user")
public class UserDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;
    @Column(name = "PSEUDO")
    private String username;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "PASSWORD")
    private String pwd;
    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DealDO> dealsDO;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TemperatureDO> temperaturesDO;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(final String pwd) {
        this.pwd = pwd;
    }

    public List<DealDO> getDealsDO() {
        return dealsDO;
    }

    public void setDealsDO(final List<DealDO> dealsDO) {
        this.dealsDO = dealsDO;
    }

    public List<TemperatureDO> getTemperaturesDO() {
        return temperaturesDO;
    }

    public void setTemperaturesDO(final List<TemperatureDO> temperaturesDO) {
        this.temperaturesDO = temperaturesDO;
    }
}
