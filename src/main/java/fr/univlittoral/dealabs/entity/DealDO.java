package fr.univlittoral.dealabs.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "tbl_deal")
public class DealDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;
    @Column(name = "TITLE")
    private String title;
    @Column(name = "SHOP_NAME")
    private String shopName;
    @Column(name = "SHOP_LINK")
    private String shopLink;
    @Column(name = "PRICE_OLD")
    private double priceOld;
    @Column(name = "PRICE_NEW")
    private double priceNew;
    @Column(name = "PROMO_CODE")
    private String promoCode;
    @Column(name = "DATE")
    private LocalDateTime date;
    @Column(name = "IMG_URL")
    private String imgUrl;
    @Column(name = "DESCRIPTION")
    private String description;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_CREATOR", referencedColumnName = "ID")
    private UserDO creator;
    @OneToMany(mappedBy = "deal", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TemperatureDO> temperatures;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(final String shopName) {
        this.shopName = shopName;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(final String shopLink) {
        this.shopLink = shopLink;
    }

    public double getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(final double priceOld) {
        this.priceOld = priceOld;
    }

    public double getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(final double priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(final String promoCode) {
        this.promoCode = promoCode;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(final String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public UserDO getCreator() {
        return creator;
    }

    public void setCreator(final UserDO creator) {
        this.creator = creator;
    }

    public List<TemperatureDO> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(final List<TemperatureDO> temperatures) {
        this.temperatures = temperatures;
    }
}
