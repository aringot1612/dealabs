package fr.univlittoral.dealabs.entity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_temperature")
public class TemperatureDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;
    @Column(name = "VALUE")
    private int value;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_USER", referencedColumnName = "ID")
    private UserDO user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_DEAL", referencedColumnName = "ID")
    private DealDO deal;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(final int value) {
        this.value = value;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(final UserDO user) {
        this.user = user;
    }

    public DealDO getDeal() {
        return deal;
    }

    public void setDeal(final DealDO deal) {
        this.deal = deal;
    }
}
